'use strict';

const express = require('express');
const moment = require('moment-timezone');
const bodyParser = require('body-parser');
let send = require('./lib/sendMsg')

require('dotenv').config()
console.log("You will hit endpoint from: "+ process.env.apiUrl)

const server = express();
server.use(bodyParser.urlencoded({
    extended: true
}));

server.use(bodyParser.json());

//variabel default
let nama
let pesan = ''
let room_id
let payload

server.post('/',async (req,res)=>{
    var dateNow = moment(new Date())
    var hour = Number(dateNow.tz('Asia/Jakarta').format('H'))
    var day = Number(dateNow.tz('Asia/Jakarta').format('e'))
    // Bot terima input dari user berupa room_id, pesan, dan nama user
    room_id = res.req.body.chat_room.qiscus_room_id
    let msg = res.req.body.message.text
    nama = res.req.body.from.fullname
    let token = res.req.body.token
    let id = res.req.body.from.id
    let isGroup = res.req.body.chat_room.is_group_chat

    if(id !== 47303 && isGroup === false) { // id which is ignored
        if (day > 0 && day < 6) {
            if (hour >= 17 || hour < 8) {
                // Message out of work hours
                pesan = '<INPUT-YOUR-MESSAGE-HERE>'
                await send.Txt(pesan, room_id, token)
                let work = '<ADDITIONAL MESSAGE>'
                await send.Txt(work, room_id, token)
            } else if(hour >= 11 && hour < 13) {
                // Message out of break time
                pesan = '<INPUT-YOUR-MESSAGE-HERE>'
                await send.Txt(pesan, room_id, token)
            }
        } else {
            // Weekend message
            pesan = '<INPUT-YOUR-MESSAGE-HERE>'
            await send.Txt(pesan, room_id, token)
            let work = '<ADDITIONAL MESSAGE>'
            await send.Txt(work, room_id, token)
        }
    }
})

server.listen((process.env.PORT || 3000), () => {
    console.log("Server is up and running..."+`${process.env.PORT}`);
});
