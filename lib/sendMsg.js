
require('dotenv').config()

const apiUrl = `${process.env.apiUrl}`

const axios = require('axios');

module.exports = {
    Txt: async (msg, room_id, token) => {
        await axios.post(apiUrl + 'post_comment', {
            access_token: token,
            topic_id: room_id,
            type: 'text',
            comment: msg
        })
    },
    Btn: (room_id, payload, token) => {
        axios.post(apiUrl + 'post_comment', {
            access_token: token,
            topic_id: room_id,
            type: 'buttons',
            payload: JSON.stringify(payload)
        });
    },
    Carousel: (room_id, payload) => {
        axios.post(apiUrl + 'post_comment', {
            access_token: token,
            topic_id: room_id,
            type: 'carousel',
            payload: JSON.stringify(payload)
        });
    },
    Card: (room_id, payload) => {
        axios.post(apiUrl + 'post_comment', {
            access_token: token,
            topic_id: room_id,
            type: 'card',
            payload: JSON.stringify(payload)
        })
    },
    File:(room_id,payload)=>{
        axios.post(apiUrl + 'post_comment', {
            access_token: token,
            topic_id: room_id,
            type: 'file_attachment',
            payload: JSON.stringify(payload)
        })
    }

}